from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Reporte Venta"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "report",
					"name": "Cierre de Caja",
					"doctype": "Sales Invoice One1",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Reporte Cuentas por Cobrar",
					"doctype": "Sales Invoice One1",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Reporte de Pagos Ventas",
					"doctype": "Sales Invoice One1",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Ventas",
					"doctype": "Sales Invoice One1",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Ventas por Producto",
					"doctype": "Sales Invoice One1",
					"is_query_report": True
				}

			]
		},

		{
			"label": _("Configuracion Venta"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Punto de Venta",
					"description": "Punto de Venta",
				},
				{
					"type": "doctype",
					"name": "Sucursal",
					"description": "Sucursal",
				}
			]
		}
	]
