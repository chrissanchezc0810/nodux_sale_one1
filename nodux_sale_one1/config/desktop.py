# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Reportes Venta",
			"color": "red",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"hidden": 1
		},

		{
			"module_name": "Venta",
			"_doctype": "Sales Invoice One1",
			"color": "#1abc9c",
			"icon": "octicon octicon-keyboard",
			"type": "link",
			"link": "List/Sales Invoice One1"
		}

	]
