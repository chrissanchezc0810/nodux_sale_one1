# Copyright (c) 2013, NODUX and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import flt
from frappe import msgprint, _

def execute(filters=None):
	if not filters: filters = {}

	invoice_list = get_invoices(filters)
	columns = get_columns(invoice_list)

	if not invoice_list:
		msgprint(_("No record found"))
		return columns, invoice_list

	data = []
	for inv in invoice_list:
		if filters.get("product"):
			product = filters.get("product")
			item = frappe.db.sql("""select item_name from `tabItem`
				where item_code=%s""",product, as_dict=1)
			filters_new =  {'inv_name': inv.name, 'product': item[0].item_name}

			lines = frappe.db.sql("""select * from `tabSales Invoice Item One`
				where parent=%(inv_name)s and item_name=%(product)s""",filters_new, as_dict=1)
		else:
			lines = frappe.db.sql("""select * from `tabSales Invoice Item One`
				where parent = %s""" ,inv.name, as_dict=1)
		for line in lines:
			row = [inv.name, inv.posting_date, line.item_name, line.qty, line.unit_price, line.subtotal, line.subtotal_imp,inv.customer_name]
			data.append(row)

	return columns, data


def get_columns(invoice_list):
	columns = [
		_("Invoice") + ":Link/Sales Invoice One1:120", _("Date") + ":Date:80",
		_("Producto") + "::240", _("Cantidad") + ":Float:80"]

	columns = columns + [_("Precio Unitario") + ":Currency/currency:100", _("Subtotal") + ":Currency/currency:100",
		_("Subtotal con Imp.") + ":Currency/currency:120", _("Cliente") + "::140"]

	return columns

def get_conditions(filters):
	conditions = ""

	if filters.get("from_date"): conditions += " and posting_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and posting_date <= %(to_date)s"

	return conditions

def get_invoices(filters):
	conditions = get_conditions(filters)

	return frappe.db.sql("""select name, posting_date, customer, customer_name
		from `tabSales Invoice One`
		where docstatus = 1 %s order by posting_date desc, name desc""" %
		conditions, filters, as_dict=1)
