// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt

frappe.ui.form.on('Punto de Venta', {
	onload: function(frm) {
		var me = this;
		// if (!frm.doc.status)
		// 	frm.doc.status = 'Draft';
		frm.refresh_fields();

		frm.call({
			method: "nodux_sale_one1.nodux_sale_one1.doctype.punto_de_venta.punto_de_venta.get_series",
			callback: function(r) {
				if(!r.exc) {
					set_field_options("sequence", r.message);
				}
			}
		});

		frm.call({
			method: "nodux_sale_one1.nodux_sale_one1.doctype.punto_de_venta.punto_de_venta.get_series",
			callback: function(r) {
				if(!r.exc) {
					set_field_options("sequence_note", r.message);
				}
			}
		});

	},

	refresh: function(frm) {

	}
});
