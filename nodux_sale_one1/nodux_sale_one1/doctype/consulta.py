from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import nowdate
from frappe import _
from frappe.utils import scrub_urls
from frappe.utils.pdf import get_pdf
from frappe import msgprint


@frappe.whitelist()
def consulta_item(codigo):
	item = frappe.db.sql("""SELECT item_name,list_price_with_tax from `tabItem`
		where item_code = %s
			and disabled=0""",
		(codigo), as_dict = 1)
	print item
