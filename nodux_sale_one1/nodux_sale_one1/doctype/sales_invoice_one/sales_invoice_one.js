// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt
$.extend(cur_frm.cscript, {
	// frappe.ui.form.on('Sales Invoice One', {
	onload: function (frm) {
		var me = this;
		var nom_fact = "";
		frappe.db.get_value("Company", {
			"company_name": frm.company
		}, "vat_number", function (r) {
			nom_fact = r.vat_number + "_" + frm.name;
			date = frm.posting_date;
			year = frm.posting_date.slice(0, -6);
			month = frm.posting_date.slice(5, -3);
			company = frm.company;
			company = company.replace(/\s/g, "_");
			company = company.toLowerCase();
		})
		cur_frm.refresh_fields();

		// if (frm.status == 'Draft' && frm.docstatus == '0') {
		// 	console.log(frm.docstatus,frm.status);
		// 	cur_frm.call({
		// 		method: "nodux_sale_one1.nodux_sale_one1.doctype.sales_invoice_one.sales_invoice_one.get_series",
		// 		args: {
		// 			'nota_venta': 0,
		// 		},
		// 		callback: function (r) {
		// 			if (!r.exc) {
		// 				cur_frm.set_value("naming_series", r.message);
		// 			}
		// 		}
		// 	});
		// 	cur_frm.call({
		// 		method: "nodux_sale_one1.nodux_sale_one1.doctype.sales_invoice_one.sales_invoice_one.get_sucursal",
		// 		callback: function (r) {
		// 			if (!r.exc) {
		// 				cur_frm.set_value("sucursal", r.message[0]);
		// 				cur_frm.set_value("dir_sucursal", r.message[1])
		// 			}
		// 		}
		// 	});
		// }
	},
	no_id: function (frm) {
		if (frm.no_id) {
			frappe.db.get_value("Customer", {
				"tax_id": frm.no_id
			}, ["customer_name", "phone", "mobile"], function (r) {
				if (typeof (r) == "undefined") {
					console.log("no hay");
					frappe.confirm(
						"El cliente no existe DESEA CREARLO",
						function () {
							var id = frm.no_id;
							frappe.set_route("Form", "Customer", "Nuevo Cliente 1", {
								"tax_id": frm.no_id
							});
							cur_frm.set_value("no_id", id)
						},
						function () {
							show_alert('NO SE ENCONTRARON COINCIDENSIAS')
						}
					)
				} else {
					cur_frm.set_value("customer", r.customer_name);
				}
			})
			cur_frm.set_value("customer_name", frm.customer);
			cur_frm.set_value("due_date", frappe.datetime.nowdate());
		}
		cur_frm.refresh_fields();
	},
	items_remove: function (frm, doc, cdt, cdn) {
		calculate_base_imponible(frm);
	},

	custom_refresh: function (frm) {
		console.log(frm.docstatus, frm.status);
		if (frm.status == 'Draft' && frm.docstatus == '1') {
			cur_frm.add_custom_button(__("Pagar"), function () {
				update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.status == 'Confirmed' && frm.docstatus == '0') {
			frm.add_custom_button(__("Pagar"), function () {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.status == 'Confirmed' && frm.docstatus == '1') {
			frm.add_custom_button(__("Pagar"), function () {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.status == 'Done' && frm.docstatus == '1') {
			frm.add_custom_button(__("Anular"), function () {
				frm.events.update_to_anulled_sale(frm);
			}).addClass("btn-primary");
		}
		cur_frm.refresh_fields();
	},
	cod_item: function (frm, cdt, cdn) {
		var d = frm;
		var barcode = "";
		if (d.cod_item) {
			if (d.items.length == 1) {
				$.each(d.items, function (index, data) {
					if (data.item_name) {} else {
						remove_child_ref(frm);
					}
				})
			}
			if (d.items.length >= 1) {
				re_tab(frm);
			} else {
				add_child_tab(frm);
			}
		}
		cur_frm.set_value("cod_item", "")
	},
	customer: function (frm) {
		if (frm.customer) {
			frappe.db.get_value("Customer", {
				"customer_name": frm.customer
			}, "tax_id", function (r) {
				cur_frm.set_value("no_id", r.tax_id);
			})
			frappe.db.get_value("Customer", {
				"customer_name": frm.customer
			}, ["email", "phone", "mobile"], function (r) {
				cur_frm.set_value("correo_cliente", r.email);
				if (r.mobile) {
					cur_frm.set_value("num_telefono", r.mobile);
				} else {
					cur_frm.set_value("num_telefono", r.phone);
				}
			})

			cur_frm.set_value("customer_name", frm.customer);
			cur_frm.set_value("due_date", frappe.datetime.nowdate());
		}
		cur_frm.refresh_fields();
	},

	nota_venta: function (frm) {

		return frappe.call({
			doc: frm.doc,
			method: "get_series",
			args: {
				"nota_venta": frm.nota_venta
			},
			callback: function (r) {
				if (!r.exc) {
					//cur_frm.set_value("naming_series", r.message);
					set_field_options("naming_series", r.message);
				}
				refresh_field("naming_series");
				cur_frm.refresh_fields();
				frm.refresh();
			}
		});
	},

	update_to_quotation_sale: function (frm) {
		return frappe.call({
			doc: frm.doc,
			method: "update_to_quotation_sale",
			freeze: true,
			callback: function (r) {
				cur_frm.refresh_fields();
				frm.refresh();
			}
		})
	},

	update_to_anulled_sale: function (frm) {
		var d = new frappe.ui.Dialog({
			title: __("Esta seguro de Anular la Venta"),
			fields: [{
					fieldname: "NO",
					"label": __("No"),
					"fieldtype": "Button"
				},
				{
					"fieldname": "coulmn_break_2",
					"fieldtype": "Column Break"
				},
				{
					fieldname: "SI",
					"label": __("Si"),
					"fieldtype": "Button"
				}
			]
		});

		d.get_input("NO").on("click", function () {
			d.hide();
		});

		d.get_input("SI").on("click", function () {
			return frappe.call({
				doc: frm.doc,
				method: "update_to_anulled_sale",
				freeze: true,
				callback: function (r) {
					d.hide();
					cur_frm.refresh_fields();
					frm.refresh();
				}
			})
		});
		d.show();
		cur_frm.refresh_fields();
		frm.refresh();

	}

	// update_to_pay_sale: function (frm) {
	// 	var me = this;
	// 	if (frm.posting_date != frm.due_date && frm.status == "Draft") {
	// 		total = 0.0
	// 	} else {
	// 		total = frm.residual_amount
	// 	}
	//
	// 	var d = new frappe.ui.Dialog({
	// 		title: __("Payment"),
	// 		fields: [{
	// 				"fieldname": "customer",
	// 				"fieldtype": "Link",
	// 				"label": __("Customer"),
	// 				options: "Customer",
	// 				reqd: 1,
	// 				label: "Customer",
	// 				"default": frm.customer_name
	// 			},
	// 			{
	// 				"fieldname": "total",
	// 				"fieldtype": "Currency",
	// 				"label": __("Total Amount"),
	// 				label: "Total Amount",
	// 				"default": total
	// 			},
	// 			{
	// 				"fieldname": "section_break",
	// 				"fieldtype": "Section Break"
	// 			},
	// 			{
	// 				"fieldname": "recibo",
	// 				"fieldtype": "Currency",
	// 				"label": __("RECIBIDO"),
	// 				label: "RECIBIDO",
	// 				"default": frm.residual_amount
	// 			},
	// 			{
	// 				"fieldname": "coulmn_break_2",
	// 				"fieldtype": "Column Break"
	// 			},
	// 			{
	// 				"fieldname": "cambio",
	// 				"fieldtype": "Read Only",
	// 				"label": __("CAMBIO"),
	// 				label: "CAMBIO",
	// 				"default": 0
	// 			},
	// 			{
	// 				"fieldname": "section_break",
	// 				"fieldtype": "Section Break"
	// 			},
	// 			{
	// 				fieldname: "pay",
	// 				"label": __("Pay"),
	// 				"fieldtype": "Button"
	// 			}
	// 		]
	// 	});
	//
	// 	d.get_input("recibo").on("change", function () {
	// 		var values = d.get_values();
	// 		var cambio = flt(values["recibo"] - values["total"])
	// 		cambio = Math.round(cambio * 100) / 100
	// 		d.set_value("cambio", cambio)
	// 	});
	//
	// 	d.get_input("pay").on("click", function () {
	// 		var values = d.get_values();
	// 		if (!values) return;
	// 		total = Math.round(values["total"] * 100) / 100
	// 		total_pay = Math.round(frm.total * 100) / 100
	//
	// 		if (total < 0) frappe.throw("Ingrese monto a pagar");
	// 		if (total < 0) frappe.throw("Monto a pagar no puede ser menor a 0");
	//
	// 		if (total > total_pay) frappe.throw("Monto a pagar no puede ser mayor al monto total de venta");
	// 		return frappe.call({
	// 			doc: frm.doc,
	// 			method: "update_to_pay_sale",
	// 			args: values,
	// 			freeze: true,
	// 			callback: function (r) {
	// 				d.hide();
	// 				refresh_field("payments");
	// 				cur_frm.refresh_fields();
	// 				frm.refresh();
	// 			}
	// 		})
	// 	});
	//
	// 	d.show();
	// 	refresh_field("payments");
	// 	cur_frm.refresh_fields();
	// 	frm.refresh();
	// },

});

frappe.ui.form.on('Sales Invoice Item One', {
	item_name: function (frm, cdt, cdn) {
		var item = frappe.get_doc(cdt, cdn);
		if (!item.item_name) {
			item.item_name = "";
		} else {
			item.item_name = item.item_name;
		}
	},
	descuento: function (frm, cdt, cdn) {

		var item = frappe.get_doc(cdt, cdn);
		item.unit_price = item.precio_descuento_fijado;
		item.unit_price_out_dcto = item.precio_descuento_fijado;
		refresh_field("items");
		cur_frm.refresh_fields();
		var d = locals[cdt][cdn];
		if (d.descuento == 1) {
			if (d.item_code) {
				d.unit_price_with_tax = d.precio_descuento_fijado;
				d.unit_price_out_dcto = d.precio_descuento_fijado / 1.12;
				d.unit_price = d.unit_price_out_dcto;
				d.subtotal_imp = d.unit_price_with_tax * d.qty;
				d.subtotal = d.unit_price * d.qty;
				refresh_field("items");
				cur_frm.refresh_fields();
				calculate_base_imponible(frm)
			}
		} else {

			args = {
				'item_code': d.item_code,
				'qty': d.qty
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_details_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							if (k == "unit_price") {
								d[k] = v;
							}
							if (k == "unit_price_with_tax") {
								d[k] = v;
							}
						});
						d.unit_price_out_dcto = d.unit_price;
						d.subtotal = d.unit_price * d.qty;
						d.subtotal_imp = d.unit_price_with_tax * d.qty;
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm)
					}
				}
			});
			//
		}

	},
	caja: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.caja == 1) {
			d.num_caja = 1;
			frappe.db.get_value("Item", {
				"barcode": d.barcode
			}, ["check_caja_fras", "cantidad"], function (r) {
				if (r.check_caja_fras == 1) {
					d.qty = r.cantidad
					d.subtotal = d.qty * d.unit_price;
					d.subtotal_imp = d.qty * d.unit_price_with_tax;
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm);
				} else {}
			})
		} else {
			d.qty = 1;
			d.subtotal = d.qty * d.unit_price;
			d.subtotal_imp = d.qty * d.unit_price_with_tax;
			refresh_field("items");
			cur_frm.refresh_fields();
			calculate_base_imponible(frm);
		}
		refresh_field("items");
		cur_frm.refresh_fields();
		calculate_base_imponible(frm);
	},
	num_caja: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.check_caja_fras == 1) {
			if (d.unidades > 0 || d.num_caja > 0) {
				var d = locals[cdt][cdn];
				if (d.num_caja != null & d.num_caja >= 0) {
					d.qty = (d.num_caja * d.cant_unidades_caja) + d.unidades;
					d = calc_cambio_qty(d, cdt, cdn);
				} else {
					if (d.num_caja == null | !d.num_caja >= 0) {
						d.qty = d.unidades;
						d.num_caja = 0;
						msgprint("<b># Cajas</b> debe ser <b>0</b> o un <b>número</b> mayor a <b>0</b>");
					}
				}
			} else {
				if (!d.unidades >= 0 || !d.num_caja >= 0) {
					d.qty = 1;
					d.num_caja = 0;
					d.unidades = 1;
					d = calc_cambio_qty(d, cdt, cdn);
					msgprint("<ul><li><b>Unidades</b> y <b># Caja</b> no pueden estar al mismo tiempo en <b>0</b></li>" +
						"<li><b>Unidades</b> y <b># Caja</b> deben ser <b>números</b> mayores o iguales a <b>0</b></li><ul>");
				}
			}
		}else{
			msgprint("<ul><li>Producto <b>"+d.item_name+" No </b> se puede vender por <b> Caja</b>");
			d.num_caja = 0;
		}


		refresh_field("items");
		cur_frm.refresh_fields();
		calculate_base_imponible(frm.doc);

		/*var d = locals[cdt][cdn];
		if (d.num_caja >= 1) {
			frappe.db.get_value("Item", {
				"barcode": d.barcode
			}, ["check_caja_fras", "cantidad", "precio_2_con_imp", "precio_2_sin_imp"], function (r) {
				d.qty = r.cantidad * d.num_caja
				if (d.qty >= r.cantidad) {
					d.unit_price = r.precio_2_sin_imp;
					d.unit_price_with_tax = r.precio_2_con_imp;
					d.subtotal = d.qty * d.unit_price;
					d.subtotal_imp = d.qty * d.unit_price_with_tax;
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm);
				} else {
					d.subtotal = d.qty * d.unit_price;
					d.subtotal_imp = d.qty * d.unit_price_with_tax;
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm);
				}
			})
		}
		refresh_field("items");
		cur_frm.refresh_fields();
		calculate_base_imponible(frm);*/
	},

	unidades: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.check_caja_fras == 1) {
			if (d.unidades > 0 || d.num_caja > 0) {
				console.log("OR");
				if (d.unidades != null & d.unidades >= 0) {
					var modn = d.unidades % d.cant_unidades_caja;
					if (modn == 0) {
						console.log("modn == 0 >" + modn);
						if (d.unidades == d.cant_unidades_caja) {
							d.num_caja++;
						}
						d.unidades = 0;
						d.qty = (d.num_caja * d.cant_unidades_caja);
						console.log("desde unidades check " + d.check_caja_fras);
						d = calc_cambio_qty(d, cdt, cdn);
					} else {
						if (modn == d.unidades) {
							console.log("modn == unidades >" + modn);
							d.qty = (d.num_caja * d.cant_unidades_caja) + d.unidades;
							console.log("desde unidades check " + d.check_caja_fras);
							d = calc_cambio_qty(d, cdt, cdn);
							console.log("antessss");

							//calculate_base_imponible(frm.doc);
							console.log("despuessss");
						} else {
							if (modn < d.cant_unidades_caja) {
								console.log("modn < cantidades caja >" + modn);
								d.num_caja += (d.unidades - modn) / d.cant_unidades_caja;
								d.unidades = modn;
								d.qty = (d.num_caja * d.cant_unidades_caja) + d.unidades;
								console.log("desde unidades check " + d.check_caja_fras);
								d = calc_cambio_qty(d, cdt, cdn);
							}
						}
					}
				}
			} else {
				console.log("else");

				if (!d.unidades >= 0 || !d.num_caja >= 0) {
					console.log("d.unidades == null");
					d.qty = 1;
					d.num_caja = 0;
					d.unidades = 1;
					d = calc_cambio_qty(d, cdt, cdn);
					msgprint("<ul><li><b>Unidades</b> y <b># Caja</b> no pueden estar al mismo tiempo en <b>0</b></li>" +
						"<li><b>Unidades</b> y <b># Caja</b> deben ser <b>números</b> mayores o iguales a <b>0</b></li><ul>");
				}
			}
		} else {
			d.qty = d.unidades;
			d = calc_cambio_qty(d, cdt, cdn);
		}

		refresh_field("items");
		cur_frm.refresh_fields();
		calculate_base_imponible(frm.doc);
	},


	barcode: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.barcode) {
			args = {
				'barcode': d.barcode
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_code_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
		cur_frm.refresh_fields();
	},

	item_code: function (frm, cdt, cdn) {
		var d = frm;
		var l = locals[cdt][cdn];
		var doc = frm.doc;
		// console.warn(doc.items.length);
		// frappe.db.get_value("Item", {
		// 	"item_code": l.item_code
		// }, ["barcode"], function (r) {
		// 	console.log("asa", r.barcode);
		// 	d.cod_item = r.barcode;
		// 	//cur_frm.set_value("cod_item", r.barcode);
		// 	if (r.barcode != null) {
		// 		add_child_tab(frm);
		// 	} else {
		// 		console.log("else", doc.items.length);
		// 	}
		// });


		//var d = frm;
		//var barcode = "";
		//if (d.cod_item) {
		//	if (d.items.length == 1) {
		//		$.each(d.items, function (index, data) {
		//			if (data.item_name) {} else {
		//				remove_child_ref(frm);
		//			}
		//		})
		//	}
		//	if (d.items.length >= 1) {
		//		re_tab(frm);
		//	} else {
		//		add_child_tab(frm);
		//	}
		//}
		//cur_frm.set_value("cod_item", "")


		var d = locals[cdt][cdn];
		var base_imponible = 0;
		var total_taxes = 0;
		var total = 0;
		var doc = frm.doc;

		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_details_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm.doc)
					}
				}
			});

			refresh_fields();
		}
	},

	uom: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];

		if (d.uom) {
			args = {
				'item_code': d.item_code,
				'uom': d.uom,
				'stock_uom': d.stock_uom,
				'conversion_factor': d.conversion_factor,
				'unit_price': d.unit_price,
				'qty': d.qty,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "update_unit",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}

	},

	subtotal_imp: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];

		if (d.uom) {
			args = {
				'item_code': d.item_code,
				'subtotal_imp': d.subtotal_imp,
				'qty': d.qty,
				'discount': d.discount,
				'unit_price': d.unit_price,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "calculate_desglose",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}

	},


	conversion_factor: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];

		if (d.uom) {
			args = {
				'item_code': d.item_code,
				'uom': d.uom,
				'stock_uom': d.stock_uom,
				'conversion_factor': d.conversion_factor,
				'unit_price': d.unit_price,
				'qty': d.qty,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "update_unit",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	},

	qty: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		//frappe.db.get_value("Item", {
		//	"barcode": d.barcode
		//}, ["check_caja_fras", "cantidad"], function (r) {
		//if (d.qty >= r.cantidad) {
		//d.num_caja = d.qty / r.cantidad;
		args = {
			'item_code': d.item_code,
			'qty': d.qty,
			'unit_price': d.unit_price
		};
		return frappe.call({
			doc: cur_frm.doc,
			method: "update_prices_qty",
			args: args,
			callback: function (r) {
				if (r.message) {
					var d = locals[cdt][cdn];
					$.each(r.message, function (k, v) {
						d[k] = v;
					});
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm.doc);
				}
			}
		});
		//}
		//})
	},

	unit_price: function (frm, cdt, cdn) {
		// if user changes the rate then set margin Rate or amount to 0
		var d = locals[cdt][cdn];
		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty,
				'unit_price': d.unit_price,
				'discount': d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
	},

	discount: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		d.unit_price = d.unit_price_out_dcto;
		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty,
				'unit_price': d.unit_price,
				'discount': d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
	},

	unit_price_out_dcto: function (frm, cdt, cdn) {
		var item = frappe.get_doc(cdt, cdn);
		item.unit_price = item.unit_price_out_dcto;
		refresh_field("items");
		cur_frm.refresh_fields();

		var d = locals[cdt][cdn];
		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty,
				'unit_price': d.unit_price,
				'discount': d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	}
})

var calculate_base_imponible = function (frm) {
	//var doc = frm.doc;
	frm.base_imponible = 0;
	frm.total_taxes = 0;
	frm.total = 0;
	frm.ahorro_factura = 0;
	console.log("calc");
	if (frm.items) {
		console.log("if items");
		$.each(frm.items, function (index, data) {
			frm.base_imponible += (data.unit_price * data.qty);
			frm.total_taxes += (data.unit_price_with_tax - data.unit_price) * data.qty;
			frm.ahorro_factura += data.descuento_total;
		})
		frm.base_imponible = Math.round(frm.base_imponible * 100) / 100
		//frm.total_taxes = Math.round(frm.total_taxes * 100) / 100
		frm.total += frm.base_imponible + frm.total_taxes;
		frm.total = Math.round(frm.total * 100) / 100;
		console.log("cbi");
	}
	console.log("ultimo p");
	frm.residual_amount = frm.total;
	refresh_field('residual_amount');
	refresh_field('base_imponible');
	refresh_field('total_taxes');
	refresh_field('total');
	refresh_field('ahorro_factura');
	console.log("final cbi");
}
var remove_child_ref = function (frm) {
	cur_frm.clear_table("items");
};
var re_tab = function (frm) {
	var d = frm;
	var line = 0;
	var i = d.items.length;
	var l = 1;
	while (i--) {
		if (d.items[i].barcode == d.cod_item && i >= 0) {
			l = 0;
		}
	}
	if (l == 1) {
		add_child_tab(frm);
	} else {
		$.each(d.items, function (index, data) {
			if (data.barcode == d.cod_item) {
				data.qty = data.qty + 1;
				data.unidades = data.qty;
				data.subtotal = data.unit_price_out_dcto * data.qty;
				data.subtotal_imp = data.unit_price_with_tax * data.qty;
				cur_frm.refresh_field("items");
				d.cod_item = "";
				cur_frm.refresh_field("cod_item");
				cur_frm.refresh_fields();
				calculate_base_imponible(frm);
			}
		})
	}
};
var add_child_tab = function (frm) {
	var d = frm;
	var qty = 1;
	var long = 0;
	frappe.db.get_value("Item", {
		"barcode": d.cod_item
	}, ["item_code", "item_name", "precio_con_descuento", "list_price_with_tax", "item_group", "list_price", "barcode", "check_caja_fras", "cantidad", "precio_2", "precio_2_con_imp", "precio_3", "precio_3_con_imp"], function (r) {
		if (r.item_code) {
			var new_row = cur_frm.add_child("items");
			new_row.check_caja_fras = r.check_caja_fras;
			new_row.item_code = r.item_code;
			new_row.barcode = r.barcode;
			new_row.item_name = r.item_name;
			new_row.unit_price_out_dcto = r.list_price;
			new_row.unit_price = r.list_price;
			new_row.unit_price_with_tax = r.list_price_with_tax;
			new_row.discount = 0.00;
			new_row.qty = 1;
			new_row.unidades = 1;
			new_row.subtotal = r.list_price * 1;
			new_row.uom = "Unidad(es)";
			new_row.conversion_factor = 1.00;
			new_row.precio_descuento_fijado = r.precio_con_descuento;
			new_row.subtotal_imp = r.list_price_with_tax * 1;
			if (r.check_caja_fras == 1) {
				new_row.cant_unidades_caja = r.cantidad;
				new_row.precio_1_sin_imp = r.list_price;
				new_row.precio_1_con_imp = r.list_price_with_tax;
				new_row.precio_2_sin_imp = r.precio_2;
				new_row.precio_2_con_imp = r.precio_2_con_imp;
				new_row.precio_3_sin_imp = r.precio_3;
				new_row.precio_3_con_imp = r.precio_3_con_imp;
				new_row.descuento_total = (new_row.qty * new_row.precio_3_con_imp) - (new_row.qty * new_row.unit_price_with_tax);
				new_row.discount = (new_row.descuento_total * 100) / (new_row.qty * new_row.precio_3_con_imp);
			}else{
				new_row.precio_1_sin_imp = r.list_price;
				new_row.precio_1_con_imp = r.list_price_with_tax;
				new_row.descuento_total = 0;
				new_row.discount = 0;
			}
			//new_row.subtotal = new_row.qty * new_row.unit_price;
			//new_row.subtotal_imp = new_row.qty * new_row.unit_price_with_tax;
			cur_frm.refresh_field("items");
			cur_frm.refresh_fields();
			calculate_base_imponible(frm);
			cur_frm.set_value("cod_item", "")
			cur_frm.refresh_field("cod_item");
		}
		refresh_field("items");
		cur_frm.refresh_fields();
	})

};

var get_barcode_of_item_code = function (item_code) {
	var ret = null;
	console.log("asa", item_code);
	frappe.db.get_value("Item", {
		"item_code": item_code
	}, ["barcode"], function (r) {
		console.log("asa", r.barcode);
		if (r.barcode != null) {
			ret = r.barcode;
			console.log("asa", r.barcode);
			return ret;
		} else {
			return ret;
		}
	});
}


var calc_cambio_qty = function (doc, cdt, cdn) {
	//var doc = frm.doc;
	console.log("antes > " + doc.check_caja_fras);
	if (doc.check_caja_fras == 1) {
		console.log("calc_cambio_qty == 1");
		if (doc.qty >= doc.cant_unidades_caja) {
			doc.unit_price = doc.precio_2_sin_imp;
			doc.unit_price_with_tax = doc.precio_2_con_imp;
			console.log("calc_cambio_qty >= cant unidades caja");
		} else {
			doc.unit_price = doc.precio_1_sin_imp;
			doc.unit_price_with_tax = doc.precio_1_con_imp;
			console.log("calc_cambio_qty else");
		}

		doc.subtotal = doc.qty * doc.unit_price;
		doc.subtotal_imp = doc.qty * doc.unit_price_with_tax;
		doc.descuento_total = (doc.qty * doc.precio_3_con_imp) - (doc.qty * doc.unit_price_with_tax);
		doc.discount = (doc.descuento_total * 100) / (doc.precio_3_con_imp * doc.qty);
	} else {
		doc.subtotal = doc.qty * doc.unit_price;
		doc.subtotal_imp = doc.qty * doc.unit_price_with_tax;
		doc.descuento_total = 0;
		doc.discount = 0;
	}

	return doc;
}
