// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt
// $.extend(cur_frm.cscript, {
frappe.ui.form.on('Sales Invoice One1', {
  refresh:function(frm){
    console.log(frm.doc.docstatus,frm.doc.status,">>>>>>>>>><");
    if (frm.doc.status == 'Draft' && frm.doc.docstatus == '1') {
        frm.add_custom_button(__("Pagar"), function () {
        frm.events.update_to_pay_sale(frm);
      }).addClass("btn-primary");
    }

    if (frm.doc.status == 'Confirmed' && frm.doc.docstatus == '0') {
      frm.add_custom_button(__("Pagar"), function () {
        frm.events.update_to_pay_sale(frm);
      }).addClass("btn-primary");
    }

    if (frm.doc.status == 'Confirmed' && frm.doc.docstatus == '1') {
      frm.add_custom_button(__("Pagar"), function () {
        frm.events.update_to_pay_sale(frm);
      }).addClass("btn-primary");
    }

    if (frm.doc.status == 'Done' && frm.doc.docstatus == '1') {
      frm.add_custom_button(__("Anular"), function () {
        frm.events.update_to_anulled_sale(frm);
      }).addClass("btn-primary");
    }
    cur_frm.refresh_fields();
  },
  update_to_pay_sale: function (frm) {
		var me = this;
		if (frm.doc.posting_date != frm.doc.due_date && frm.doc.status == "Draft") {
			total = 0.0
		} else {
			total = frm.doc.residual_amount
		}

		var d = new frappe.ui.Dialog({
			title: __("Payment"),
			fields: [{
					"fieldname": "customer",
					"fieldtype": "Link",
					"label": __("Customer"),
					options: "Customer",
					reqd: 1,
					label: "Customer",
					"default": frm.doc.customer_name
				},
				{
					"fieldname": "total",
					"fieldtype": "Currency",
					"label": __("Total Amount"),
					label: "Total Amount",
					"default": total
				},
				{
					"fieldname": "section_break",
					"fieldtype": "Section Break"
				},
				{
					"fieldname": "recibo",
					"fieldtype": "Currency",
					"label": __("RECIBIDO"),
					label: "RECIBIDO",
					"default": frm.doc.residual_amount
				},
				{
					"fieldname": "coulmn_break_2",
					"fieldtype": "Column Break"
				},
				{
					"fieldname": "cambio",
					"fieldtype": "Read Only",
					"label": __("CAMBIO"),
					label: "CAMBIO",
					"default": 0
				},
				{
					"fieldname": "section_break",
					"fieldtype": "Section Break"
				},
				{
					fieldname: "pay",
					"label": __("Pay"),
					"fieldtype": "Button"
				}
			]
		});

		d.get_input("recibo").on("change", function () {
			var values = d.get_values();
			var cambio = flt(values["recibo"] - values["total"])
			cambio = Math.round(cambio * 100) / 100
			d.set_value("cambio", cambio)
		});

		d.get_input("pay").on("click", function () {
			var values = d.get_values();
			if (!values) return;
			total = Math.round(values["total"] * 100) / 100
			total_pay = Math.round(frm.total * 100) / 100

			if (total < 0) frappe.throw("Ingrese monto a pagar");
			if (total < 0) frappe.throw("Monto a pagar no puede ser menor a 0");

			if (total > total_pay) frappe.throw("Monto a pagar no puede ser mayor al monto total de venta");
			return frappe.call({
				doc: frm.doc,
				method: "update_to_pay_sale",
				args: values,
				freeze: true,
				callback: function (r) {
					d.hide();
					refresh_field("payments");
					cur_frm.refresh_fields();
					frm.refresh();
				}
			})
		});

		d.show();
		refresh_field("payments");
		cur_frm.refresh_fields();
		frm.refresh();
	},
});
