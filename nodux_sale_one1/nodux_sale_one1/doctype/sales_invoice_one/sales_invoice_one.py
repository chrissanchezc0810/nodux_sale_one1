# -*- coding: utf-8 -*-
# Copyright (c) 2015, NODUX and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import nowdate
from frappe import _
from frappe.utils import scrub_urls
from frappe.utils.pdf import get_pdf
from frappe import msgprint

class SalesInvoiceOne(Document):
	def before_save(self):
		self.residual_amount = self.total

	def update_to_anulled_sale(self):
		#agregar permisos de usuario
		for line in self.items:
			product = frappe.get_doc("Item", line.item_code)
			if product.es_servicio == 1:
				pass
			else:
				if product.total == None:
					product.total = line.qty
				else:
					product.total = product.total + line.qty
					product.save()
		self.status="Anulled"
		self.save()

	def update_to_pay_sale(self, args=None):
		party = frappe.get_doc("Customer", self.customer_name)
		if self.correo_cliente:
			party.email = self.correo_cliente
			party.save()
		if self.docstatus == 0 and self.status == "Draft":
			self.status = "Confirmed"
			self.docstatus = 1
			self.residual_amount = self.total
			self.save()
			user = frappe.session.user
			temp = frappe.permissions.get_doc_permissions(self, False,user)
			#permiso = temp.get("amend")
			permiso = frappe.db.sql("""select out_stock from `tabPunto de Venta` where user = %s""", (user), as_dict = 1)
			permiso = "1"
			out_stock = "1"
			for item in self.items:
				if item.item_code:
					product = frappe.get_doc("Item", item.item_code)
					if product.es_servicio == 1:
						pass
					else:
						if permiso == "1":
							#out_stock = frappe.db.sql("""select out_stock from `tabPunto de Venta` where user = %s""", (user), as_dict = 1)
							if product.total == None:
								if out_stock == "1":
									product.total = item.qty*(-1)
									product.save()
								else:
									frappe.throw(("El producto: {0} no se encuentra disponible en stock").format(item.item_name))
							else:
								if product.total<0:
									if out_stock == "1":
										product.total = product.total-item.qty
										product.save()
										msgprint(("El producto {0} se ha vendido en negativo").format(item.item_name))
									else:
										frappe.throw(("El producto  {0} no se encuentra disponible en stock").format(item.item_name))
								elif product.total < item.qty:
									if out_stock == "1":
										product.total = product.total-item.qty
										product.save()
										msgprint(("El producto {0} se ha vendido en negativo").format(item.item_name))
									else:
										frappe.throw(("No cuenta con suficiente stock del producto {0}").format(item.item_name))
								else:
									product.total = product.total-item.qty
									product.save()
						else:
							if product.total == None:
								frappe.throw(("El producto : {0} no se encuentra disponible en stock").format(item.item_name))
							else:
								if product.total<0:
									frappe.throw(("El producto: {0} no se encuentra disponible en stock").format(item.item_name))
								elif product.total < item.qty:
									frappe.throw(("No cuenta con suficiente stock del producto {0}").format(item.item_name))
								else:
									product.total = product.total-item.qty
									product.save()

		customer = args.get('customer')
		total = args.get('total')

		if self.paid_amount > 0:
			self.paid_amount = self.paid_amount + total
		else:
			self.paid_amount = total

		residual_amount = self.total - self.paid_amount
		self.residual_amount = residual_amount
		if round(residual_amount) == 0:
			self.status ="Done"
		else:
			self.status = "Confirmed"

		self.save()

		payment = frappe.get_doc({
			"doctype":"Sales Invoice Payment One",
			"date": nowdate(),
			"amount": total,
			"parent": self.name,
			"parenttype": "Sales Invoice One1",
			"docstatus": 1,
			"parentfield": "payments"
		})
		payment.save()

	def set_paid_amount_sale(self):
		paid_amount = 0.0
		base_paid_amount = 0.0
		for data in self.payments:
			data.base_amount = flt(data.amount*self.conversion_rate, self.precision("base_paid_amount"))
			paid_amount += data.amount
			base_paid_amount += data.base_amount
		self.paid_amount = paid_amount
		self.base_paid_amount = base_paid_amount

	def get_item_details_sale(self, args=None, for_update=False):
		item = frappe.db.sql("""select stock_uom, description, image, item_name,
			list_price, list_price_with_tax, barcode, tax,conversion from `tabItem`
			where name = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('item_code'), nowdate()), as_dict = 1)
		if not item:
			frappe.throw(_("Item {0} is not active or end of life has been reached").format(args.get("item_code")))

		item = item[0]

		if item.conversion == 0:
			conversion = 1
		else:
			conversion= item.conversion

		ret = {
			'uom'			      	: item.stock_uom,
			'description'		  	: item.description,
			'item_name' 		  	: item.item_name,
			'qty'					: 1,
			'barcode'				: item.barcode,
			'unit_price'			: item.list_price,
			'unit_price_out_dcto'	: item.list_price,
			'unit_price_with_tax'	: item.list_price_with_tax,
			'subtotal_imp'			: item.list_price_with_tax,
			'subtotal'				: item.list_price,
			'conversion_factor'		: conversion
		}

		#update uom
		if args.get("uom") and for_update:
			ret.update(get_uom_details(args.get('item_code'), args.get('uom'), args.get('qty')))

		return ret

	def update_unit(self, args=None, for_update=False):
		item = frappe.db.sql("""select tax, list_price from `tabItem`
			where name = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('item_code'), nowdate()), as_dict = 1)

		item = item[0]

		if args.get('uom') == args.get("stock_uom"):
			unit_price = args.get("unit_price")
			subtotal_imp = args.get("unit_price") * args.get("qty")
			unit_price_with_tax = args.get("unit_price")
		else:
			if args.get("conversion_factor") != 0:
				unit_price_with_tax = float(args.get("unit_price")  / args.get("conversion_factor"))
				unit_price = float(args.get("unit_price") / args.get("conversion_factor"))
			else:
				unit_price_with_tax = args.get("unit_price")
				unit_price = args.get("unit_price")

			subtotal_imp = args.get("unit_price") * args.get("qty")


		if args.get("uom") and args.get("stock_uom") and args.get("conversion_factor"):
			if args.get('uom') == args.get("stock_uom"):
				pass
			else:
				conversion_factor = args.get("conversion_factor")
				qty = args.get("qty")
				unit_price = args.get("unit_price")
				if args.get("conversion_factor") != 0:
					unit_price =  float(float(unit_price * qty) / conversion_factor)
				else:
					unit_price =  (unit_price * qty)


				if item.tax == "IVA 0%":
					unit_price_with_tax = unit_price
					subtotal_imp = qty * unit_price
				elif item.tax == "IVA 12%":
					unit_price_with_tax= unit_price * (1.12)
					subtotal_imp = unit_price_with_tax * qty
				elif item.tax == "No aplica impuestos":
					unit_price_with_tax = unit_price
					subtotal_imp = qty * unit_price
				else:
					unit_price_with_tax = unit_price
					subtotal_imp = qty * unit_price

				ret = {
					'unit_price'			: unit_price,
					'subtotal_imp'			: subtotal_imp,
					'subtotal'				: unit_price*args.get("qty"),
					'unit_price_with_tax'	: unit_price_with_tax
				}

				return ret

	def calculate_desglose(self, args=None, for_update=False):
		item = frappe.db.sql("""select tax, list_price from `tabItem`
			where name = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('item_code'), nowdate()), as_dict = 1)

		item = item[0]

		subtotal_imp = float(args.get("subtotal_imp"))
		qty = float(args.get('qty'))
		unit_price_with_tax = float(subtotal_imp / qty)
		discount = args.get("discount")
		unit_price = args.get("unit_price")
		if discount > 0:
			discount_percentage = float((float(discount)/float(100))+1)
		elif discount == 0:
			discount_percentage = 0
		else:
			frappe.throw(_("El porcentaje de descuento debe ser mayor a cero"))

		if item.tax == "IVA 0%":
			if discount_percentage > 0:
				unit_price = unit_price_with_tax/discount_percentage
			else:
				unit_price = unit_price_with_tax
			unit_price_d = unit_price_with_tax
		elif item.tax == "IVA 12%":
			if discount_percentage > 0:
				unit_price = (unit_price_with_tax / (1.12))/discount_percentage
			else:
				unit_price = (unit_price_with_tax / (1.12))
			unit_price_d = (unit_price_with_tax / (1.12))
		elif item.tax == "No aplica impuestos":
			if discount_percentage > 0:
				unit_price = unit_price_with_tax/discount_percentage
			else:
				unit_price = unit_price_with_tax
			unit_price_d = unit_price_with_tax
		else:
			unit_price_d = unit_price_with_tax

		ret = {
			'unit_price'			: unit_price,
			'unit_price_out_dcto'	: unit_price_d,
			'unit_price_with_tax'	: unit_price_with_tax,
			'subtotal'				: unit_price * qty
		}

		return ret

	def update_prices_qty(self, args=None, for_update=False):
		item = frappe.db.sql("""select tax from `tabItem`
			where name = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('item_code'), nowdate()), as_dict = 1)

		item = item[0]
		unit_price = args.get("unit_price")
		subtotal_imp = args.get("unit_price") * args.get("qty")
		unit_price_with_tax = args.get("unit_price")

		if args.get("qty") and args.get("unit_price"):
			unit_price = args.get("unit_price")
			qty = args.get("qty")

			if item.tax == "IVA 0%":
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price
			elif item.tax == "IVA 12%":
				unit_price_with_tax= unit_price * (1.12)
				subtotal_imp = unit_price_with_tax * qty
			elif item.tax == "No aplica impuestos":
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price
			else:
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price

		ret = {
			'unit_price'			: unit_price,
			'subtotal_imp'			: subtotal_imp,
			'subtotal'				: unit_price * args.get("qty"),
			'unit_price_with_tax'	: unit_price_with_tax
		}

		return ret


	def update_prices_discount(self, args=None, for_update=False):
		item = frappe.db.sql("""select tax from `tabItem`
			where name = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('item_code'), nowdate()), as_dict = 1)

		item = item[0]
		unit_price = args.get("unit_price_out_dcto")
		subtotal_imp = args.get("unit_price_out_dcto") * args.get("qty")
		unit_price_with_tax = args.get("unit_price_out_dcto")
		discount = args.get("discount")


		if args.get("qty") and args.get("unit_price_out_dcto"):
			if discount > 0:
				unit_price = unit_price - float(unit_price*(float(discount)/float(100)))
			elif discount == 0:
				unit_price = args.get("unit_price_out_dcto")
			else:
				frappe.throw(_("El porcentaje de descuento debe ser mayor a cero"))

			qty = args.get("qty")
			if item.tax == "IVA 0%":
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price
			elif item.tax == "IVA 12%":
				unit_price_with_tax= unit_price * (1.12)
				subtotal_imp = unit_price_with_tax * qty
			elif item.tax == "No aplica impuestos":
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price
			else:
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price

		ret = {
			'unit_price'			: unit_price,
			'subtotal_imp'			: subtotal_imp,
			'subtotal'				: unit_price * args.get("qty"),
			'unit_price_with_tax'	: unit_price_with_tax,
		}

		return ret

	def update_prices_sale(self, args=None, for_update=False):
		item = frappe.db.sql("""select tax from `tabItem`
			where name = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('item_code'), nowdate()), as_dict = 1)

		item = item[0]
		unit_price = args.get("unit_price")
		subtotal_imp = args.get("unit_price")
		unit_price_with_tax = args.get("unit_price")
		discount = args.get("discount")

		if args.get("qty") and args.get("unit_price"):
			if discount > 0:
				unit_price = unit_price - float(unit_price*(float(discount)/float(100)))
			elif discount == 0:
				unit_price = args.get("unit_price")
			else:
				frappe.throw(_("El porcentaje de descuento debe ser mayor a cero"))

			qty = args.get("qty")
			subtotal_imp = unit_price * qty
			if item.tax == "IVA 0%":
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price
			elif item.tax == "IVA 12%":
				unit_price_with_tax= unit_price * (1.12)
				subtotal_imp = unit_price_with_tax * qty
			elif item.tax == "No aplica impuestos":
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price
			else:
				unit_price_with_tax = unit_price
				subtotal_imp = qty * unit_price

		ret = {
			'unit_price'			: unit_price,
			'subtotal_imp'			: subtotal_imp,
			'subtotal'				: unit_price * args.get("qty"),
			'unit_price_with_tax'	: unit_price_with_tax
		}

		return ret

	def get_item_code_sale(self, args=None, serial_no=None):
		item = frappe.db.sql("""select stock_uom, description, image, item_name,
			list_price, item_code, list_price_with_tax from `tabItem`
			where barcode = %s
				and disabled=0
				and (end_of_life is null or end_of_life='0000-00-00' or end_of_life > %s)""",
			(args.get('barcode'), nowdate()), as_dict = 1)

		if not item:
			frappe.throw(_("No existe producto con codigo de barra {0}").format(args.get("barcode")))

		item = item[0]

		ret = {
			'uom'			      	: item.stock_uom,
			'description'		  	: item.description,
			'item_name' 		  	: item.item_name,
			'item_code'				: item.item_code,
			'qty'					: 1,
			'unit_price'			: item.list_price,
			'unit_price_with_tax'	: item.list_price_with_tax,
			'subtotal_imp'			: item.list_price_with_tax,
			'subtotal'				: item.list_price,
		}

		return ret

	def get_series(self):
		if self.nota_venta == 1:
			user = frappe.session.user
			sequence_note = frappe.db.sql("""select sequence_note from `tabPunto de Venta`
				where user = %s""",
				(user), as_dict = 1)

			if sequence_note == []:
				return frappe.get_meta("Sales Invoice One1").get_field("naming_series").options or ""
			else:
			 	return sequence_note[0].sequence_note
		else:
			user = frappe.session.user
			sequence = frappe.db.sql("""select sequence from `tabPunto de Venta`
				where user = %s""",
				(user), as_dict = 1)

			if sequence == []:
				return frappe.get_meta("Sales Invoice One1").get_field("naming_series").options or ""
			else:
			 	return sequence[0].sequence

@frappe.whitelist()
def get_series(nota_venta):
	if nota_venta == 1:
		user = frappe.session.user
		sequence = frappe.db.sql("""select sequence_note from `tabPunto de Venta`
			where user = %s""",
			(user), as_dict = 1)

		if sequence == []:
			return frappe.get_meta("Sales Invoice One1").get_field("naming_series").options or ""
		else:
		 	return sequence[0].sequence
	else:
		user = frappe.session.user
		sequence = frappe.db.sql("""select sequence from `tabPunto de Venta`
			where user = %s""",
			(user), as_dict = 1)

		if sequence == []:
			return frappe.get_meta("Sales Invoice One1").get_field("naming_series").options or ""
		else:
		 	return sequence[0].sequence

@frappe.whitelist()
def get_sucursal():
	user = frappe.session.user
	sucursal = frappe.db.sql("""select sucursal from `tabPunto de Venta`
		where user = %s""",
		(user), as_dict = 1)
	print "La sucursal", sucursal
	if sucursal == []:
		pass
	else:
		data_sucursal = frappe.db.sql("""select name_suc, address from `tabSucursal`
			where name_suc = %s""",
			(sucursal[0].sucursal), as_dict = 1)
		return data_sucursal[0].name_suc, data_sucursal[0].address
